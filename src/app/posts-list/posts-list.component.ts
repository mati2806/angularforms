import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {
  posts: Object;
  postId: number = -1;
  errorMessage: string;
  formModel: FormGroup;
  selected: number = -1;

  constructor(private _http: HttpService) 
  { 
    this.formModel = new FormGroup({
      title: new FormControl('', Validators.required),
      body: new FormControl('', [Validators.required, Validators.maxLength(20)])
    });
  }

  ngOnInit(){
    this._http.getPosts().subscribe(data => {
      this.posts = data;
      console.log(this.posts);
    });
  }


  update(){
    var selectedPost = this.posts[this.selected];
    this._http.updatePost(selectedPost.id, this.formModel.get('title').value, this.formModel.get('body').value).subscribe({
      next: data => {
          this.postId = data.id;
          selectedPost.id = data.id;
          selectedPost.title = data.title;
          selectedPost.body = data.body;
          console.log(data);
      },
      error: error => {
          this.errorMessage = error.message;
          console.error('There was an error!', error);
      }
    });
  }

  select(which: number): void {
    this.selected = which;
    this.formModel.controls['title'].setValue(this.posts[this.selected].title);
    this.formModel.controls['body'].setValue(this.posts[this.selected].body);
  }

  get title() { return this.formModel.get('title'); }

  get body() { return this.formModel.get('body'); }

}
